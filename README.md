# WebChat REST API #

## Opcao 1 - Para executar a api rest utilizando docker

### Setup

Clonar o projeto https://bitbucket.org/moacsjr/webchat-web. Os dois projetos devem estar no mesmo diretório. Exemplo clone os dois projetos dentro da pasta webchat-workspace.
Configure no seu hosts o endereço do docker para o nome webchatapi

```
192.168.99.100    webchatapi
```

- Geralmente o ip do docker é 192.168.99.100 para confirmar digite o comando abaixo em um terminal docker:

```
docker-machine ip
```


### Inicie o Docker

A partir do terminal do Docker acesse o diretório dos fontes do projeto (webchat-api/) e rode o comando


```
mvnw install -Dmaven.test.skip=true
docker-compose up --build
```
- Usamos -Dmaven.test.skip pois os testes de integração podem falhar se o mongo não estiver instalado e rodando localmente. Além disso a api está configurada inicialmente para se conectar no host mongodb. Veja config descrito na Opção 2 detalhada mais abaixo. 

O docker irá aprovisionar e executar 3 VMs uma com o mongodb, outra com a api rest java e uma com o frontend rodando com Node.js.


## Opção 2 - Para executar a api backend localmente

#### Setup

Configure no seu hosts o endereço do localhost para o nome webchatapi

127.0.0.1   webchatapi

#### Mongodb

Criar a entrada 'mongodb' no hosts da máquina local apontando para 127.0.0.1

127.0.0.1    mongodb

Ou ajuste o application.yaml para apontar para o endereço correto da sua instalação de mongodb.

## Créditos

Para construir esse projeto eu consultei e utilizei informações de diversas fontes nos quais destaco as seguintes:

https://www.toptal.com/java/rest-security-with-jwt-spring-security-and-java
https://github.com/szerhusenBC/jwt-spring-security-demo


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact