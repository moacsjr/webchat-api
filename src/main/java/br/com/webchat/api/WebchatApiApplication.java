package br.com.webchat.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class WebchatApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebchatApiApplication.class, args);
	}
}
