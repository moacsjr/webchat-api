package br.com.webchat.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
//@Order(Ordered.HIGHEST_PRECEDENCE + 99)
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/queue", "/user");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry
	        .addEndpoint("/webchat-websocket")
	        .setAllowedOrigins("*")
	        .withSockJS();
    }

//    /**
//     * Para suportar autenticação baseada em token conforme detalhado nos links abaixo:
//     * https://github.com/spring-projects/spring-framework/blob/master/src/asciidoc/web-websocket.adoc#token-based-authentication
//     * https://github.com/sockjs/sockjs-client/issues/196
//     * https://github.com/jhipster/generator-jhipster/issues/1370
//     */
//    @Override
//    public void configureClientInboundChannel(ChannelRegistration registration) {
//
//      registration.setInterceptors(new ChannelInterceptorAdapter() {
//
//          @Override
//          public Message<?> preSend(Message<?> message, MessageChannel channel) {
//
//              StompHeaderAccessor accessor =
//                  MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//
//              List tokenList = accessor.getNativeHeader("X-Authorization");
//              accessor.removeNativeHeader("X-Authorization");
//              System.out.println("Chegou aqui");
//
//              String token = null;
//              if(tokenList != null && tokenList.size() > 0) {
//                token = (String) tokenList.get(0);
//              }
//
//              System.out.println(token);
//
//              if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//                  Principal user = token == null ? null : new Principal() {
//                      @Override
//                      public String getName() {
//                        return "guest@webchat.com";
//                      }
//                  };
//                  accessor.setUser(user);
//              }
//
//              return message;
//          }
//      });
//    }

}
