package br.com.webchat.api.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.webchat.api.dto.ChatMessage;
import br.com.webchat.api.repository.HistoricoMensagensRepository;

@Controller
public class ChatMessageRestController {

	private final SimpMessagingTemplate messagingTemplate;
	private final HistoricoMensagensRepository history;

	@Autowired
	public ChatMessageRestController(SimpMessagingTemplate messagingTemplate, HistoricoMensagensRepository history) {
		this.messagingTemplate = messagingTemplate;
		this.history = history;
	}

	@MessageMapping("/chat")
    public void greeting(ChatMessage message, Principal user) throws Exception {
		message.setFrom(user.getName());
		message.setUid(UUID.randomUUID());
		message.setTime(new Date());
		message.setRead(false);
		this.messagingTemplate.convertAndSendToUser(message.getTo(), "/queue/chat-updates", message);
		this.messagingTemplate.convertAndSendToUser(message.getFrom(), "/queue/chat-updates", message);

		//gera historico de mensagens
		history.save(message);
    }

	@MessageMapping("/contactupdate")
    public void contact(ChatMessage message, Principal user) throws Exception {
		message.setFrom(user.getName());
		message.setUid(UUID.randomUUID());
		message.setTime(new Date());
		message.setRead(false);
		this.messagingTemplate.convertAndSendToUser(message.getTo(), "/queue/contact-updates", message);
		this.messagingTemplate.convertAndSendToUser(message.getFrom(), "/queue/contact-updates", message);
    }

	@MessageMapping("/login")
    public void login(ChatMessage message, Principal user) throws Exception {
		message.setFrom(user.getName());
		message.setUid(UUID.randomUUID());
		message.setTime(new Date());
		message.setRead(false);
		this.messagingTemplate.convertAndSend("/queue/login-updates", message);
    }

	@RequestMapping(value="/chat/{username}/history", method={ RequestMethod.GET }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<ChatMessage> getFriendship(@PathVariable("username") String username){
		return history.fetchUnReadedMessages(username, false);
	}


}
