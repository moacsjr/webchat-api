package br.com.webchat.api.controller;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.webchat.api.core.FriendshipStatus;
import br.com.webchat.api.core.WebchatException;
import br.com.webchat.api.dto.Friendship;
import br.com.webchat.api.exception.UnauthorizedRequestException;
import br.com.webchat.api.repository.FriendshipRepository;

@RestController()
public class FriendshipRestController {

	private static final Logger log = org.slf4j.LoggerFactory.getLogger(FriendshipRestController.class);
	private FriendshipRepository repository;

	@Autowired
	public FriendshipRestController(FriendshipRepository repository) {
		this.repository = repository;
	}

	@RequestMapping(value="/friendship/{username}", method={ RequestMethod.GET }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Friendship> getFriendship(@PathVariable("username") String username, Principal user){

		if(user==null || !user.getName().equals(username)){
			throw new UnauthorizedRequestException();
		}

		return this.repository.findByUsername(username);
	}

	@RequestMapping(value="/friendship/{username}/pending", method={ RequestMethod.GET }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Friendship> getPendingFriendshipRequest(@PathVariable("username") String username, Principal user){

		if(user==null || !user.getName().equals(username)){
			throw new UnauthorizedRequestException();
		}

		return this.repository.findByFriendIdAndStatus(username, FriendshipStatus.PENDENTE);
	}

	@RequestMapping(value="/friendship/{username}/request", method={ RequestMethod.POST }, consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void requestFriendship(@PathVariable("username") String username, @RequestBody Friendship request, Principal user){

		if(user==null || !user.getName().equals(username)){
			throw new UnauthorizedRequestException();
		}

		try{
			request.setStatus(FriendshipStatus.PENDENTE);
			this.repository.save(request);
		}catch (org.springframework.dao.DuplicateKeyException e) {
			throw new WebchatException("Solicitação de amizada já enviada/");
		}
		log.info("User {} requested friendship to {}", request.getUsername(), request.getFriendId());
	}

	@RequestMapping(value="/friendship/{username}/accept/{email}", method={ RequestMethod.POST }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void acceptRequest(@PathVariable("username") String username, @PathVariable("email") String email, Principal user){
		if(user==null || !user.getName().equals(username)){
			throw new UnauthorizedRequestException();
		}

		Friendship friendshipRequest = this.repository.findByUsernameAndFriendId(email,username);
		if(friendshipRequest==null){
			throw new ResourceNotFoundException();
		}

		Friendship friendship = null;

		//verify if we already have a friendship connection
		Friendship friendshipConnection = this.repository.findByUsernameAndFriendId(username,email);
		if(friendshipConnection != null){
			//reuse this connection
			friendship = friendshipConnection;
		}else{
			friendship = new Friendship(); //create a new friendship connection
		}

		friendshipRequest.setStatus(FriendshipStatus.ACEITO);

		friendship.setFriendId(friendshipRequest.getUsername());
		friendship.setUsername(friendshipRequest.getFriendId());
		friendship.setStatus(FriendshipStatus.ACEITO);
		repository.save(friendship); //save/update my new friendship
		repository.save(friendshipRequest);
		log.info("Friendship accepted! User {} accepts friendship request from {}", username, email);

	}

	@RequestMapping(value="/friendship/{username}/block/{email}", method={ RequestMethod.POST }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void blockRequest(@PathVariable("username") String username, @PathVariable("email") String email, Principal user){
		Friendship myFriendship = this.repository.findByUsernameAndFriendId(username, email);
		Friendship myFriendsRequest = this.repository.findByUsernameAndFriendId(email, username);

		if(myFriendship==null && myFriendsRequest==null){
			throw new ResourceNotFoundException();
		}

		//verifica se já somos amigos
		if(myFriendship!=null && myFriendsRequest!=null && myFriendship.getStatus().equals(FriendshipStatus.ACEITO)){
			//verifica se quem solicitou o bloqueio
			if(user==null || !user.getName().equals(myFriendship.getUsername())){
				throw new UnauthorizedRequestException();
			}
			myFriendship.setStatus(FriendshipStatus.BLOQUEANDO);
			myFriendsRequest.setStatus(FriendshipStatus.BLOQUEADO);
		}else{ // então é uma solicitação de amizade que está sendo rejeitada
			//verifica se quem solicitou o bloqueio
			if(user==null || !user.getName().equals(myFriendsRequest.getFriendId())){
				throw new UnauthorizedRequestException();
			}

			myFriendsRequest.setStatus(FriendshipStatus.BLOQUEADO);

			//criamos uma nova conexão de amizada, porém bloqueada.
			//Fazemos isso para lembrar-mos que esse usuário foi bloqueado
			//isso permitirá que a amizade seja refeita no futuro
			Friendship newFriendship = new Friendship();
			newFriendship.setFriendId(myFriendsRequest.getUsername());
			newFriendship.setUsername(myFriendsRequest.getFriendId());
			newFriendship.setStatus(FriendshipStatus.BLOQUEANDO);  // isso permite que eu desbloqueie o usuário no futuro
			repository.save(newFriendship); //save my new blocked friendship
		}


		if(myFriendship != null){
			this.repository.save(myFriendship);
		}
		if(myFriendsRequest != null){
			this.repository.save(myFriendsRequest);
		}
		log.info("Friendship Rejected! User {} blocked user from {}", username, email);
	}

	@RequestMapping(value="/friendship/{username}/unblock/{email}", method={ RequestMethod.POST }, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void unBlockRequest(@PathVariable("username") String username, @PathVariable("email") String email, Principal user){
		Friendship f1 = this.repository.findByUsernameAndFriendId(username, email);
		//verifica se f1 foi quem solicitou o bloqueio
		if(!user.getName().equals(f1.getUsername())){
			throw new UnauthorizedRequestException();
		}
		if(FriendshipStatus.BLOQUEANDO.equals(f1.getStatus())){
			Friendship f2 = this.repository.findByUsernameAndFriendId(email, username);
			f1.setStatus(FriendshipStatus.ACEITO);
			f2.setStatus(FriendshipStatus.ACEITO);
			this.repository.save(f1);
			this.repository.save(f2);
		}else{
			throw new WebchatException("Desbloqueio não permitido. Você foi bloqueado pelo contato e somente ele pode retirar o bloqueio");
		}
		log.info("Friendship Refreshed! User {} UN-blocked user {}", username, email);
	}



}
