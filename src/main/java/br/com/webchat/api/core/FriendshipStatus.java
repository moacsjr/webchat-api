package br.com.webchat.api.core;

public enum FriendshipStatus {
	
	ACEITO, //a requisiçao foi aceita
	PENDENTE,  // a requisiçao está aguarando aceite do usuário
	BLOQUEADO,  // você foi bloqueado ou a requisição não foi aceita
	BLOQUEANDO  // você bloqueou a amizada, e só você poderá desbloquear

}
