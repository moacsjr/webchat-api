package br.com.webchat.api.core;

public class WebchatException extends RuntimeException{
	
	public WebchatException(String message) {
		super(message);
	}
	
	public WebchatException(String message, Exception ex) {
		super(message, ex);
	}

}
