package br.com.webchat.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.webchat.api.core.FriendshipStatus;

@Document
@CompoundIndex(name="frndshp_unique", def="{username: 1, friendId: 1}", unique=true)
public class Friendship {

	@Id
	private String id;

	@NotBlank
	@Email
	private String username;

	@Email
	@NotBlank
	private String friendId;

	@NotBlank
	private String friendNickName;

	@NotNull
	private FriendshipStatus status;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}

	public FriendshipStatus getStatus() {
		return status;
	}

	public void setStatus(FriendshipStatus status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFriendNickName() {
		return friendNickName;
	}

	public void setFriendNickName(String friendNickName) {
		this.friendNickName = friendNickName;
	}



}
