package br.com.webchat.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.webchat.api.core.FriendshipStatus;
import br.com.webchat.api.dto.Friendship;

@RepositoryRestResource(exported=true)
public interface FriendshipRepository extends MongoRepository<Friendship, String> {

	List<Friendship> findByUsername(String username);

	@Query("{'username': ?0, 'friendId': ?1}")
	Friendship findByUsernameAndFriendId(String username, String friendId);

	List<Friendship> findByFriendIdAndStatus(String friendId, FriendshipStatus status);

}
