package br.com.webchat.api.repository;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import br.com.webchat.api.core.FriendshipStatus;
import br.com.webchat.api.core.WebchatException;
import br.com.webchat.api.dto.Friendship;

@Component
@RepositoryEventHandler(Friendship.class)
public class FriendshipRepositoryEventHandler {
	
	@Autowired
	private Validator validator;
	
	@HandleBeforeSave 
	public void handleBeforeSave(Friendship p) {
		validate(p);
	}
	

	@HandleBeforeCreate
	public void handleBeforeCreate(Friendship p){
		p.setStatus(FriendshipStatus.PENDENTE);
		validate(p);
	}
	
	
	private void validate(Friendship p) {
		Set<ConstraintViolation<Friendship>> constraintViolations = validator.validate( p );
		if(!constraintViolations.isEmpty()){
			ConstraintViolation<Friendship> erro = constraintViolations.iterator().next();
			throw new WebchatException("FriendshipRequest inválido: "+ erro.getPropertyPath() + " - " + erro.getMessage());
		}
	}

}
