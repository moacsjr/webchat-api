package br.com.webchat.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.webchat.api.dto.ChatMessage;

@RepositoryRestResource(exported=false)
public interface HistoricoMensagensRepository extends MongoRepository<ChatMessage, String>{

	@Query("{'to': ?0, 'read':?1}")
	List<ChatMessage> fetchUnReadedMessages(String to, Boolean read);

	@Query("{'to': ?0, 'from':?0}")
	List<ChatMessage> fetchAllMessages(String to);

}
