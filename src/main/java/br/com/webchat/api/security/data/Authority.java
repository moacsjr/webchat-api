package br.com.webchat.api.security.data;

import javax.validation.constraints.NotNull;

public class Authority {

	public Authority() {
	}

    public Authority(Long id, AuthorityName name) {
		super();
		this.id = id;
		this.name = name;
	}

	private Long id;

    @NotNull
    private AuthorityName name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthorityName getName() {
        return name;
    }

    public void setName(AuthorityName name) {
        this.name = name;
    }

}