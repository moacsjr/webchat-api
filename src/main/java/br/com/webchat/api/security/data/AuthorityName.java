package br.com.webchat.api.security.data;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}