package br.com.webchat.api.security.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.webchat.api.security.data.User;

/**
 * Created by stephan on 20.03.16.
 */
public interface UserRepository extends MongoRepository<User, String> {
    //User findByUsername(String username);
    User findByEmail(String email);
}
