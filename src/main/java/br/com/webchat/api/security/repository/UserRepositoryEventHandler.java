package br.com.webchat.api.security.repository;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.webchat.api.core.WebchatException;
import br.com.webchat.api.security.data.Authority;
import br.com.webchat.api.security.data.AuthorityName;
import br.com.webchat.api.security.data.User;

@Component
@RepositoryEventHandler(User.class)
public class UserRepositoryEventHandler {
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	public PasswordEncoder passwordEncoder;
	
	@HandleBeforeSave 
	public void handleBeforeSave(User p) {
	    
	}
	
	@HandleBeforeCreate
	public void handleBeforeCreate(User user){
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setEnabled(true);
		user.setUsername(user.getEmail());
		Set<ConstraintViolation<User>> constraintViolations = validator.validate( user );
		if(!constraintViolations.isEmpty()){
			ConstraintViolation<User> erro = constraintViolations.iterator().next();
			throw new WebchatException("Usuário inválido: "+ erro.getPropertyPath() + " - " + erro.getMessage());
		}
		  user.setUsername(user.getEmail());
		  user.setAuthorities(new ArrayList<>());
		  user.getAuthorities().add(new Authority(null, AuthorityName.ROLE_USER));
	}
	
	@HandleBeforeDelete
	public void handleBeforeDelete(User p){
		
	}

}
