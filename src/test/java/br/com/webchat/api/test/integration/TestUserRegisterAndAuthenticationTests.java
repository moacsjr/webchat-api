package br.com.webchat.api.test.integration;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.webchat.api.WebchatApiApplication;
import br.com.webchat.api.security.JwtAuthenticationRequest;
import br.com.webchat.api.security.data.Authority;
import br.com.webchat.api.security.data.AuthorityName;
import br.com.webchat.api.security.data.User;
import br.com.webchat.api.security.repository.UserRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={WebchatApiApplication.class}, initializers = ConfigFileApplicationContextInitializer.class)
public class TestUserRegisterAndAuthenticationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

	@Autowired
	private WebApplicationContext context;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	public PasswordEncoder passwordEncoder;

	private MockMvc mvc;

	@Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = java.util.Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	
		User user = userRepository.findByEmail("guest@webchat.com");
		if(user == null){
			user = new User("guest", "webchat.com", passwordEncoder.encode("secret"), "guest@webchat.com");
			user.setUsername(user.getEmail());
			user.setAuthorities(new ArrayList<>());
			user.getAuthorities().add(new Authority(null, AuthorityName.ROLE_USER));
			userRepository.save(user);
		}		
	}


	@Test
	public void createUser() throws Exception {
		String userName = "testeUser_"+  new Double(Math.floor(Math.random()*100)).intValue();
		User user = new User(userName, "webchat.com", "secret", userName+"@webchat.com");
		String request = json(user);
		mvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON_UTF8).content(request.getBytes())).andExpect(status().is(201));
	}

	private String userMail(String userName) {
		return userName+"@webchat.com";
	}

	@Test
	public void getAListOfUsers() throws Exception {
		mvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void guestUserMustExist() throws Exception {
		mvc.perform(get("/users/guest@webchat.com").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	@Test
	public void guestUserNotFound() throws Exception {
		mvc.perform(get("/users/notfound-user@webchat.com").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
	}

	@Test
	public void autheticateWithGuestUser() throws Exception {
		String request = json(new JwtAuthenticationRequest("guest@webchat.com", "secret"));
		mvc.perform(post("/auth")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(request.getBytes()))
		.andExpect(status().isOk());
	}

	@Test(expected=Exception.class)
	public void autheticateWithGuestUserSenhaErrada() throws Exception {
		String request = json(new JwtAuthenticationRequest("guest@webchat.com", "senha_errada"));
		Exception ex = mvc.perform(post("/auth")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(request.getBytes()))
		.andReturn().getResolvedException();
	}

	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
