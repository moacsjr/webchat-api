package br.com.webchat.api.test.integration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import br.com.webchat.api.config.WebSocketConfig;

@SpringBootApplication(scanBasePackageClasses={WebSocketConfig.class})
@ComponentScan(basePackages="br.com.webchat.api")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,
		MongoAutoConfiguration.class,
		MongoDataAutoConfiguration.class,
		MongoRepositoriesAutoConfiguration.class, RepositoryRestMvcAutoConfiguration.class, SecurityAutoConfiguration.class })
public class TestWebchatApiApplication {

	public static void main(String[] args) {
		//SpringApplication.run(WebchatApiApplication.class, args);
	}
}