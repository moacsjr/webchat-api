package br.com.webchat.api.test.unit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import br.com.webchat.api.controller.FriendshipRestController;
import br.com.webchat.api.core.FriendshipStatus;
import br.com.webchat.api.core.WebchatException;
import br.com.webchat.api.dto.Friendship;
import br.com.webchat.api.exception.UnauthorizedRequestException;
import br.com.webchat.api.repository.FriendshipRepository;

public class TestFriendshipController {

	@Rule
    public ExpectedException thrown = ExpectedException.none();

	@Test
	public void requestFriendship(){

		FriendshipRepository repository = mock(FriendshipRepository.class);
		FriendshipRestController controller = new FriendshipRestController(repository );

		String username="guest@webchat.com.br";
		Friendship request = new Friendship();
		request.setUsername(username);
		request.setFriendId("teste@webchat.com");
		request.setFriendNickName("teste");

		controller.requestFriendship("guest@webchat.com.br", request, getUserPrincipal(username) );

		verify(repository, times(1)).save(request);
	}

	@Test
	public void getFriendship(){
		FriendshipRepository repository = mock(FriendshipRepository.class);
		String username = "user";

		List<Friendship> friends = new ArrayList<>();
		Friendship f1 = getFriendTeste1(username);
		Friendship f2 = getFriendTeste2(username);
		friends.add(f1);
		friends.add(f2);

		when(repository.findByUsername(username)).thenReturn(friends );
		FriendshipRestController controller = new FriendshipRestController(repository );

		List<Friendship> result = controller.getFriendship(username, getUserPrincipal(username));

		assertNotNull(result);
		assertFalse(result.isEmpty());

	}

	private Friendship getFriendTeste1(String username) {
		Friendship f1 = mock(Friendship.class);
		when(f1.getFriendId()).thenReturn("teste1@webchat.com");
		when(f1.getFriendNickName()).thenReturn("teste1");
		when(f1.getUsername()).thenReturn(username);
		when(f1.getStatus()).thenReturn(FriendshipStatus.ACEITO);
		return f1;
	}

	private Friendship getFriendTeste1(String username, String friendId) {
		Friendship f1 = mock(Friendship.class);
		when(f1.getFriendId()).thenReturn(friendId);
		when(f1.getFriendNickName()).thenReturn("teste1");
		when(f1.getUsername()).thenReturn(username);
		when(f1.getStatus()).thenReturn(FriendshipStatus.ACEITO);
		return f1;
	}

	private Friendship getFriendTeste2(String username) {
		Friendship f1 = mock(Friendship.class);
		when(f1.getFriendId()).thenReturn("teste2@webchat.com");
		when(f1.getFriendNickName()).thenReturn("teste2");
		when(f1.getUsername()).thenReturn(username);
		when(f1.getStatus()).thenReturn(FriendshipStatus.ACEITO);
		return f1;
	}

	@Test
	public void acceptRequest(){
		FriendshipRepository repository = mock(FriendshipRepository.class);
		String username = "user";
		String friendId = "teste1@webchat.com";
		Friendship f1 = getFriendTeste1(username);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f1 );

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.acceptRequest(username, friendId, getUserPrincipal(username));
		verify(f1).setStatus(FriendshipStatus.ACEITO);
	}

	@Test
	public void acceptRequestFriendshipNotFound(){
		thrown.expect(org.springframework.data.rest.webmvc.ResourceNotFoundException.class);
		FriendshipRepository repository = mock(FriendshipRepository.class);
		String username = "user";
		String friendId = "teste1@webchat.com";
		Friendship f1 = getFriendTeste1(username);
		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1 );

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.acceptRequest(username, friendId, getUserPrincipal(username));
	}

	@Test
	public void blockFriendship(){
		String username = "user";
		String friendId = "teste1@webchat.com";

		Friendship f1 = getFriendTeste1( username, friendId);
		Friendship f2 = getFriendTeste1( friendId, username);
		UserPrincipal userPrincipal = getUserPrincipal(username);

		FriendshipRepository repository = mock(FriendshipRepository.class);

		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f2);

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.blockRequest(username, friendId, userPrincipal);

		verify(f1).setStatus(FriendshipStatus.BLOQUEANDO);
		verify(f2).setStatus(FriendshipStatus.BLOQUEADO);
	}

	public void blockFriendshipNotFound(){
		thrown.expect(org.springframework.data.rest.webmvc.ResourceNotFoundException.class);

		String username = "user";
		String friendId = "teste1@webchat.com";

		Friendship f1 = null;
		Friendship f2 = getFriendTeste1( friendId, username);
		UserPrincipal userPrincipal = getUserPrincipal(username);

		FriendshipRepository repository = mock(FriendshipRepository.class);

		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f2);

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.blockRequest(username, friendId, userPrincipal);

	}
	@Test
	public void blockFriendshipDiferentUser(){
		thrown.expect(UnauthorizedRequestException.class);

		String username = "user";
		String friendId = "teste1@webchat.com";

		Friendship f1 = getFriendTeste1( username, friendId);
		Friendship f2 = getFriendTeste1( friendId, username);
		UserPrincipal userPrincipal = getUserPrincipal("anonimousUser");

		FriendshipRepository repository = mock(FriendshipRepository.class);

		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f2);

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.blockRequest(username, friendId, userPrincipal);

		verify(f1).setStatus(FriendshipStatus.BLOQUEANDO);
		verify(f2).setStatus(FriendshipStatus.BLOQUEADO);
	}

	@Test
	public void unBlockFriendship(){
		String username = "user";
		String friendId = "teste1@webchat.com";

		Friendship f1 = getFriendTeste1( username, friendId);
		Friendship f2 = getFriendTeste1( friendId, username);

		when(f1.getStatus()).thenReturn(FriendshipStatus.BLOQUEADO);
		when(f1.getStatus()).thenReturn(FriendshipStatus.BLOQUEANDO);

		UserPrincipal userPrincipal = getUserPrincipal(username);

		FriendshipRepository repository = mock(FriendshipRepository.class);

		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f2);

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.unBlockRequest(username, friendId, userPrincipal);

		verify(f1).setStatus(FriendshipStatus.ACEITO);
		verify(f2).setStatus(FriendshipStatus.ACEITO);
	}
	@Test
	public void unBlockFriendshipAlredyBlocked(){
		thrown.expect(WebchatException.class);
		String username = "user";
		String friendId = "teste1@webchat.com";

		Friendship f1 = getFriendTeste1( username, friendId);
		Friendship f2 = getFriendTeste1( friendId, username);

		when(f1.getStatus()).thenReturn(FriendshipStatus.BLOQUEANDO);
		when(f1.getStatus()).thenReturn(FriendshipStatus.BLOQUEADO);

		UserPrincipal userPrincipal = getUserPrincipal(username);

		FriendshipRepository repository = mock(FriendshipRepository.class);

		when(repository.findByUsernameAndFriendId(username, friendId)).thenReturn(f1);
		when(repository.findByUsernameAndFriendId(friendId, username)).thenReturn(f2);

		FriendshipRestController controller = new FriendshipRestController(repository );
		controller.unBlockRequest(username, friendId, userPrincipal);

		verify(f1).setStatus(FriendshipStatus.ACEITO);
		verify(f2).setStatus(FriendshipStatus.ACEITO);
	}

	private UserPrincipal getUserPrincipal(String username) {
		return new UserPrincipal() {

			@Override
			public String getName() {
				return username;
			}
		};
	}

}
